package pw.soopr.pixelbosses;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import pw.soopr.pixelbosses.objects.Boss;
import pw.soopr.pixelbosses.objects.LivingBoss;
import pw.soopr.pixelbosses.objects.Urn;

import java.util.*;

/**
 * Created by Aaron.
 */
public class BossListener implements Listener{

    private static final int TOP_PLAYERS = 5;

    @EventHandler
    public void onDie(EntityDeathEvent e){
        Iterator<LivingBoss> livingBossIterator = PixelBosses.getI().getLivingBosses().iterator();
        while(livingBossIterator.hasNext()){
            LivingBoss b = livingBossIterator.next();
            if(b.getEntity() != null && b.getLivingMinions() != null && !b.getLivingMinions().isEmpty()){
                for(Zombie zombie : b.getLivingMinions()){
                    if(zombie.getEntityId() == e.getEntity().getEntityId()){
                        e.setDroppedExp(0);
                        e.getDrops().clear();
                    }
                }
            }
            if(b.getEntity() != null && b.getMount().isValid()){
                if(b.getMount().getEntityId() == e.getEntity().getEntityId()){
                    e.setDroppedExp(0);
                    e.getDrops().clear();
                }
            }
            if(b.getEntity() != null && b.getEntity().getUniqueId() == e.getEntity().getUniqueId()){
                for(String general : b.getTemplate().getCmds()){
                    Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),general);
                }
                Map<Player,Double> sorted = sortByValues(b.getTopFive());
                int i = 0;
                for(Player p : sorted.keySet()){
                    if(i >= TOP_PLAYERS) break;
                    for(String cmd : b.getTemplate().getKillerCmds()){
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(),cmd.replace("<player>",p.getName()));
                    }
                    i++;
                }
                if(b.getLivingMinions() != null && !b.getLivingMinions().isEmpty()){
                    for(Zombie zombie : b.getLivingMinions()){
                        zombie.setHealth(0.0);
                    }
                }
                if(b.getMount() != null && b.getMount().isValid()){
                    b.getMount().setHealth(0);
                }
                e.setDroppedExp(0);
                e.getDrops().clear();
                if(b.getTemplate().getDrops().next() != null) e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), b.getTemplate().getDrops().next());
                livingBossIterator.remove();
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDmg(EntityDamageByEntityEvent e){
        if(e.isCancelled() || e.getDamage() <= 0){
            return;
        }
        Player p = null;
        if(e.getDamager() instanceof Player){
            p = (Player) e.getDamager();
        }else if(e.getDamager() instanceof Projectile){
            if(((Projectile)e.getDamager()).getShooter() instanceof Player) p = (Player)((Projectile)e.getDamager()).getShooter();
        }
        if(p == null){
            for(LivingBoss b : PixelBosses.getI().getLivingBosses()){
                if(b.getLivingMinions() != null){
                    for(Zombie zombie : b.getLivingMinions()) if(zombie.getEntityId() == e.getDamager().getEntityId()){
                        e.setDamage(e.getDamage() * b.getTemplate().getMinion().getDmgMod());
                    }
                }
                if(b.getMount() != null && b.getMount().isValid()){
                    if(b.getMount().getEntityId() == e.getDamager().getEntityId()){
                        e.setDamage(e.getDamage() * b.getTemplate().getMount().getDmgMod());
                    }
                }
                if(b.getEntity() != null && b.getEntity().getEntityId() == e.getDamager().getEntityId()){
                    e.setDamage(e.getDamage() * b.getTemplate().getDmgMod());
                }

                if(b.getEntity() != null && b.getMount() != null){
                    if(e.getDamager().getEntityId() == b.getMount().getEntityId() && e.getEntity().getEntityId() == b.getEntity().getEntityId()) e.setCancelled(true);
                }
            }
        }

        if(p != null){
            for(LivingBoss b : PixelBosses.getI().getLivingBosses()){
                if(b.getEntity() != null && b.getEntity().getUniqueId() == e.getEntity().getUniqueId()){
                    b.incDamage(p,e.getDamage());
                }
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e){
        if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
            if(e.getPlayer().getItemInHand() != null){
                for(Urn urn : PixelBosses.getI().getUrns()){
                    if(urn.getIcon().isSimilar(e.getPlayer().getItemInHand())){
                        e.setCancelled(true);
                        int i = isSpawnValid(e.getPlayer());
                        if(i == 0){
                            urn.applyRandomCommands(e.getPlayer());
                            removeInventoryItems(e.getPlayer().getInventory(), e.getPlayer().getItemInHand(), 1);
                            Boss req = urn.getChances().next();
                            req.spawn(e.getPlayer().getLocation());
                            Bukkit.broadcastMessage(req.getBroadcast().replace("<nl>","\n"));
                            //new FancyMessage(req.getBroadcast()).send(Bukkit.getOnlinePlayers());
                            PixelBosses.getI().getF("successfullySpawned").replace("<boss>",req.getName()).sendTo(e.getPlayer());
                            return;
                        }else if(i == 1){
                            PixelBosses.getI().getF("notAllowedToSpawnHere").sendTo(e.getPlayer());
                            return;
                        }else if(i == 2){
                            PixelBosses.getI().getF("bossAlreadyExistsHere").sendTo(e.getPlayer());
                            return;
                        }
                    }
                }
            }
        }
    }

    private HashMap sortByValues(WeakHashMap<Player,Double> map) {
        List list = new LinkedList(map.entrySet());
        // Defined Custom Comparator here
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        // Here I am copying the sorted list in HashMap
        // using LinkedHashMap to preserve the insertion order
        HashMap sortedHashMap = new LinkedHashMap<Player,Double>();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedHashMap.put(entry.getKey(), entry.getValue());
        }
        return sortedHashMap;
    }

    public int isSpawnValid(Player p){
        ApplicableRegionSet set = WorldGuardPlugin.inst().getRegionManager(p.getWorld()).getApplicableRegions(p.getLocation());
        if(set == null || set.getRegions().isEmpty()) return 1;
        for(ProtectedRegion region : set.getRegions()){
            for(String s : PixelBosses.getI().getRegions()){
                if(region.getId().equalsIgnoreCase(s)){
                    for(LivingBoss boss : PixelBosses.getI().getLivingBosses()){
                        ApplicableRegionSet bossRegions = WorldGuardPlugin.inst().getRegionManager(boss.getEntity().getWorld()).getApplicableRegions(boss.getEntity().getLocation());
                        for(ProtectedRegion bossRegion: bossRegions.getRegions()){
                            if(bossRegion.getId().equals(region.getId())) return 2;
                        }
                    }
                    return 0;
                }
            }
        }
        return 1;
    }

    public void removeInventoryItems(PlayerInventory inv, ItemStack type, int amount) {
        for (ItemStack is : inv.getContents()) {
            if (is != null && is.isSimilar(type)) {
                int newamount = is.getAmount() - amount;
                if (newamount > 0) {
                    is.setAmount(newamount);
                    break;
                } else {
                    inv.remove(is);
                    amount = -newamount;
                    if(amount == 0) break;
                }
            }
        }
    }
}
