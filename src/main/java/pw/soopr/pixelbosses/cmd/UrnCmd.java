package pw.soopr.pixelbosses.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.soopr.pixelbosses.PixelBosses;
import pw.soopr.pixelbosses.objects.Urn;

/**
 * Created by Aaron.
 */
@Cmd(name = "urn",usage = "/urn give <player> <type> <quantity>",min = 4,perm = "pixelbosses.urn",description = "Give an urn to spawn a boss.")
public class UrnCmd implements ICmd{
    @Override
    public boolean execute(CommandSender sender, ArgsSet args) throws PlayerException {
        Player p = Bukkit.getServer().getPlayer(args.get(1).getAsString());

        if(p == null){
            throw new PlayerException("The specified player is not online!");
        }

        Urn urn = null;
        for(Urn urnObj : PixelBosses.getI().getUrns()){
            if(urnObj.getType().equalsIgnoreCase(args.get(2).getAsString())) urn = urnObj;
        }

        if(urn == null) {
            throw new PlayerException("Invalid urn type specified!");
        }

        int amt;
        try{
            amt = Integer.parseInt(args.get(3).getAsString());
        }catch (NumberFormatException e){
            throw new PlayerException("Invalid amount specified!");
        }

        if(amt <= 0) throw new PlayerException("Invalid amount specified!");

        urn.giveToPlayer(p,amt);
        PixelBosses.getI().getF("gaveUrn").replace("<player>",p.getName()).replace("<amount>",amt).replace("<type>",urn.getType()).sendTo(sender);

        return true;
    }
}
