package pw.soopr.pixelbosses.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.Executor;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.soopr.pixelbosses.objects.Boss;
import pw.soopr.pixelbosses.PixelBosses;

/**
 * Created by Aaron.
 * /boss spawn [id]
 */
@Cmd(name = "boss",usage = "/boss spawn [id]",min = 2,perm = "pixelbosses.boss",executors = Executor.PLAYER,description = "Spawn a boss at your location.")
public class BossSpawnCmd implements ICmd{
    @Override
    public boolean execute(CommandSender sender, ArgsSet args) throws PlayerException {
        Player p = (Player) sender;
        Boss boss = PixelBosses.getI().getBoss(args.get(1).getAsString());
        if(boss == null){
            p.sendMessage(ChatColor.RED + "Invalid boss specified.");
            return false;
        }

        boss.spawn(p.getLocation());
        p.sendMessage(ChatColor.GREEN + "Spawned boss with id: " + boss.getId());
        Bukkit.broadcastMessage(boss.getBroadcast().replace("<nl>","\n"));
        //new FancyMessage(boss.getBroadcast()).send(Bukkit.getOnlinePlayers());

        return true;
    }
}
