package pw.soopr.pixelbosses.objects.bosscomponents;

import lombok.Value;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.Set;

/**
 * Created by Aaron.
 */
@Value
public class Mount {
    private double health;
    private double dmgMod;
    private Set<PotionEffect> effects;
    private ItemStack[] armor;
    private ItemStack hand;
    private EntityType type;
}
