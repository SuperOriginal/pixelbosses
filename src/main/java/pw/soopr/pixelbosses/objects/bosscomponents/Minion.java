package pw.soopr.pixelbosses.objects.bosscomponents;

import lombok.Value;
import org.bukkit.entity.EntityType;
import org.bukkit.potion.PotionEffect;

import java.util.Set;

/**
 * Created by Aaron.
 */
@Value
public class Minion {
    private double health;
    private double dmgMod;
    private int amt;
    private Set<PotionEffect> effects;
}
