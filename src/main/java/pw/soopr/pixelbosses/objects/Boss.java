package pw.soopr.pixelbosses.objects;

import lombok.Data;
import org.bukkit.Location;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import pw.soopr.pixelbosses.*;
import pw.soopr.pixelbosses.objects.bosscomponents.Minion;
import pw.soopr.pixelbosses.objects.bosscomponents.Mount;

import java.util.*;

/**
 * Created by Aaron.
 * config:

 swagId:
 type: ZOMBIE
 health: 300
 name: '&e&l meme'
 armor: #if applicable
 head: IRON_BLOCK: 0
 chest: null
 leggings: null
 boots: null
 hand: #if applicable DIAMOND_SWORD
 broadcast: shit to broadcast on spawn
 commands:
 - shit to
 - execute on death
 killerCommands:
 - commands to execute for top
 - 5 damages of the boss when it
 - dies, <player>
 */
@Data
public class Boss {
    private String id;
    private EntityType type;
    private double health;
    private String name;
    private ItemStack[] armor;
    private ItemStack hand;
    private String broadcast;
    private List<String> cmds;
    private List<String> killerCmds;
    private RandomCollection<ItemStack> drops;
    private double dmgMod;
    private int life;
    private Minion minion;
    private Set<PotionEffect> effects;
    private Mount mount;
    private boolean specialFlag = false;

    public LivingEntity spawn(Location loc){
        Entity entity = loc.getWorld().spawnEntity(loc,this.getType());
        if(entity instanceof LivingEntity){
            LivingEntity lEntity = (LivingEntity) entity;
            lEntity.setMaxHealth(this.getHealth());
            lEntity.setHealth(this.getHealth());

            lEntity.getEquipment().setHelmet(this.getArmor()[0]);
            lEntity.getEquipment().setChestplate(this.getArmor()[1]);
            lEntity.getEquipment().setLeggings(this.getArmor()[2]);
            lEntity.getEquipment().setBoots(this.getArmor()[3]);
            lEntity.getEquipment().setItemInHand(this.getHand());
            lEntity.setCustomName(this.getName());
            lEntity.setCustomNameVisible(true);
            if(lEntity instanceof Rabbit && specialFlag){
                Rabbit r  = (Rabbit) lEntity;
                r.setRabbitType(Rabbit.Type.THE_KILLER_BUNNY);
            }
            else if(lEntity instanceof Skeleton && specialFlag){
                Skeleton skeleton = (Skeleton) lEntity;
                skeleton.setSkeletonType(Skeleton.SkeletonType.WITHER);
            }
            for(PotionEffect effect : effects) lEntity.addPotionEffect(effect);
            List<Zombie> minions = null;
            if(minion != null){
                minions = new ArrayList<>();
                for(int i = 0; i < minion.getAmt(); i++){
                    Zombie min = (Zombie)loc.getWorld().spawnEntity(loc,EntityType.ZOMBIE);
                    min.setBaby(true);
                    min.setVillager(false);
                    min.setMaxHealth(minion.getHealth());
                    min.setHealth(minion.getHealth());
                    min.getEquipment().setHelmet(this.getArmor()[0]);
                    min.getEquipment().setChestplate(this.getArmor()[1]);
                    min.getEquipment().setLeggings(this.getArmor()[2]);
                    min.getEquipment().setBoots(this.getArmor()[3]);
                    for(PotionEffect effect : minion.getEffects()) min.addPotionEffect(effect);
                    minions.add(min);
                }
            }

            LivingEntity mountEntity = null;
            if(mount != null){
                mountEntity = (LivingEntity) loc.getWorld().spawnEntity(loc,mount.getType());
                mountEntity.setMaxHealth(mount.getHealth());
                mountEntity.setHealth(mount.getHealth());
                mountEntity.getEquipment().setHelmet(mount.getArmor()[0]);
                mountEntity.getEquipment().setChestplate(mount.getArmor()[1]);
                mountEntity.getEquipment().setLeggings(mount.getArmor()[2]);
                mountEntity.getEquipment().setBoots(mount.getArmor()[3]);
                for(PotionEffect effect : mount.getEffects()) mountEntity.addPotionEffect(effect);
                lEntity.setPassenger(mountEntity);
            }
            PixelBosses.getI().getLivingBosses().add(new LivingBoss(this,new WeakHashMap<>(),lEntity,lEntity.getHealth(),loc,life > 0 ? System.currentTimeMillis() + life*1000 : null,minions,mountEntity));

            return lEntity;
        }
        return null;
    }

    public LivingEntity spawnWithoutAddition(LivingBoss b, Location loc){
        Entity entity = loc.getWorld().spawnEntity(loc,this.getType());
        if(entity instanceof LivingEntity){
            LivingEntity lEntity = (LivingEntity) entity;
            lEntity.setMaxHealth(this.getHealth());
            lEntity.setHealth(this.getHealth());

            lEntity.getEquipment().setHelmet(this.getArmor()[0]);
            lEntity.getEquipment().setChestplate(this.getArmor()[1]);
            lEntity.getEquipment().setLeggings(this.getArmor()[2]);
            lEntity.getEquipment().setBoots(this.getArmor()[3]);

            lEntity.getEquipment().setItemInHand(this.getHand());
            lEntity.setCustomName(this.getName());
            lEntity.setCustomNameVisible(true);
            if(lEntity instanceof Rabbit && specialFlag){
                Rabbit r  = (Rabbit) lEntity;
                r.setRabbitType(Rabbit.Type.THE_KILLER_BUNNY);
            }
            else if(lEntity instanceof Skeleton && specialFlag){
                Skeleton skeleton = (Skeleton) lEntity;
                skeleton.setSkeletonType(Skeleton.SkeletonType.WITHER);
            }
            for(PotionEffect effect : effects) lEntity.addPotionEffect(effect);
            b.setEntity(lEntity);
            return lEntity;
        }
        return null;
    }
}
