package pw.soopr.pixelbosses.objects;

import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import pw.soopr.pixelbosses.RandomCollection;
import pw.soopr.pixelbosses.objects.Boss;

import java.util.List;

/**
 * Created by Aaron.
 */
@Data
public class Urn {
    private String type;
    private RandomCollection<Boss> chances;
    private RandomCollection<List<String>> itemChances;
    private ItemStack icon;

    @Override
    public boolean equals(Object obj){
        if(obj == this) return true;
        if(obj instanceof ItemStack){
            ItemStack stack = (ItemStack) obj;
            if(stack.isSimilar(icon)) return true;
        }
        return false;
    }

    public void giveToPlayer(Player p,int amount){
        ItemStack urn = icon.clone();
        urn.setAmount(amount);
        p.getInventory().addItem(urn);
    }

    public void applyRandomCommands(Player p){
        if(itemChances == null || itemChances.next() == null) return;
        itemChances.next().forEach(cmd -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd.replace("<player>", p.getName())));
    }
}
