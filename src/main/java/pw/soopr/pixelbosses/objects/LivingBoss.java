package pw.soopr.pixelbosses.objects;

import lombok.*;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import pw.soopr.pixelbosses.objects.Boss;

import java.util.List;
import java.util.WeakHashMap;

/**
 * Created by Aaron.
 */
@AllArgsConstructor
public class LivingBoss {
    @Getter
    private Boss template;
    @Getter
    private WeakHashMap<Player,Double> topFive;
    @Setter @Getter
    private LivingEntity entity;
    @Setter@Getter
    private double currentHealth;
    @Setter@Getter
    private Location lastLocation;
    @Getter@Setter
    private Long expiryDate;
    @Getter
    private List<Zombie> livingMinions;

    @Getter
    private LivingEntity mount;

    public void incDamage(Player p, double dmg){
        double i = 0;
        if(topFive.containsKey(p)) i = topFive.get(p);
        i+=dmg;
        topFive.put(p,i);
    }
}
