package pw.soopr.pixelbosses;

import io.ibj.JLib.JPlug;
import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.TimePeriod;
import io.ibj.JLib.TimeUnit;
import io.ibj.JLib.file.ResourceFile;
import io.ibj.JLib.file.ResourceReloadHook;
import io.ibj.JLib.file.YAMLFile;
import io.ibj.JLib.utils.Colors;
import io.ibj.JLib.utils.ItemMetaFactory;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import pw.soopr.pixelbosses.cmd.BossSpawnCmd;
import pw.soopr.pixelbosses.cmd.UrnCmd;
import pw.soopr.pixelbosses.objects.Boss;
import pw.soopr.pixelbosses.objects.LivingBoss;
import pw.soopr.pixelbosses.objects.Urn;
import pw.soopr.pixelbosses.objects.bosscomponents.Minion;
import pw.soopr.pixelbosses.objects.bosscomponents.Mount;

import java.util.*;

/**
 * Created by Aaron.
 *  swagId:
 type: ZOMBIE
 health: 300
 name: '&e&l meme'
 armor: #if applicable
 head: IRON_BLOCK: 0
 chest: null
 leggings: null
 boots: null
 hand: #if applicable DIAMOND_SWORD
 broadcast: shit to broadcast on spawn
 commands:
 - shit to
 - execute on death
 killerCommands:
 - commands to execute for top
 - 5 damages of the boss when it
 - dies, <player>
 */
public class PixelBosses extends JPlug{
    @Getter
    private Set<Boss> bosses;
    @Getter
    private static PixelBosses i;

    @Getter
    private Set<Urn> urns;

    @Getter
    private Set<LivingBoss> livingBosses;

    @Getter
    private Set<String> regions;


    @Override
    public void onModuleEnable(){
        i = this;

        bosses = new HashSet<>();
        urns = new HashSet<>();
        livingBosses = new HashSet<>();
        regions = new HashSet<>();

        registerResource(new YAMLFile(this, "config.yml", new ResourceReloadHook() {
            @Override
            public void onReload(ResourceFile file) {
                getLogger().info("reloading lists");
                FileConfiguration config = ((YAMLFile)file).getConfig();
                Iterator<LivingBoss> livingBossIterator = PixelBosses.getI().getLivingBosses().iterator();
                while(livingBossIterator.hasNext()){
                    LivingBoss b = livingBossIterator.next();
                    b.getEntity().remove();
                    if(b.getLivingMinions() != null){
                        b.getLivingMinions().forEach(m -> m.remove());
                    }
                    if(b.getMount() != null) b.getMount().remove();
                    livingBossIterator.remove();
                }
                regions.clear();
                bosses.clear();
                config.getStringList("regions").forEach(s -> regions.add(s));
                for(String key : config.getConfigurationSection("bosses").getKeys(false)){
                    Boss add = new Boss();
                    add.setId(key);
                    add.setName(Colors.colorify(config.getString("bosses."+key+".name")));
                    add.setLife(config.getInt("bosses."+key+".life"));
                    EntityType type;
                    if(config.getString("bosses."+key+".type").equalsIgnoreCase("KILLER_RABBIT")){
                        type = EntityType.RABBIT;
                        add.setSpecialFlag(true);
                    }else if(config.getString("bosses."+key+".type").equalsIgnoreCase("WITHER_SKELETON")){
                        type = EntityType.SKELETON;
                        add.setSpecialFlag(true);
                    } else{
                        type = EntityType.valueOf(config.getString("bosses." + key + ".type"));
                        if (type == null) {
                            getLogger().severe("Invalid type specified for boss: " + key);
                            continue;
                        }
                    }
                    add.setType(type);

                    double health = config.getDouble("bosses."+key + ".health");
                    if(health <= 0.0){
                        getLogger().severe("Invalid health specified for boss: " + key);
                        continue;
                    }
                    add.setHealth(health);
                    add.setArmor(parseArmor(config.getConfigurationSection("bosses."+key+".armor")));
                    add.setHand(parseHand(config.getConfigurationSection("bosses."+key)));
                    add.setBroadcast(Colors.colorify(config.getString("bosses."+key+".broadcast")));
                    add.setCmds(config.getStringList("bosses."+key+".commands"));
                    add.setKillerCmds(config.getStringList("bosses."+key+".killerCommands"));
                    add.setDmgMod(config.getDouble("bosses." + key + ".damageMod"));

                    RandomCollection<ItemStack> dropList = new RandomCollection<>();

                    ConfigurationSection grandDrops = config.getConfigurationSection("bosses."+key + ".drops");
                    for(String drop : grandDrops.getKeys(false)){
                        ConfigurationSection drops = grandDrops.getConfigurationSection(drop);
                        ItemStack dropItem = new ItemStack(Material.valueOf(drops.getString("material")),drops.getInt("amount"),(short)drops.getInt("data"));
                        if(drops.getStringList("enchantments") != null) {
                            for (String enchant : drops.getStringList("enchantments")) {
                                String[] comps = enchant.split(":");
                                dropItem.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(comps[0])), Integer.parseInt(comps[1]));
                            }
                        }
                        ItemMetaFactory factory = new ItemMetaFactory(dropItem);
                        drops.getStringList("lore").forEach(s -> factory.addToLore(Colors.colorify(s)));
                        factory.setDisplayName(Colors.colorify(drops.getString("displayName")));
                        factory.set();

                        dropList.add(drops.getDouble("chance"),dropItem);
                    }
                    add.setEffects(parsePotionEffects(config.getConfigurationSection("bosses."+key)));
                    add.setDrops(dropList);

                    if(config.getConfigurationSection("bosses."+key +".minion") != null && !config.getConfigurationSection("bosses."+key +".minion").getKeys(false).isEmpty()){
                        ConfigurationSection minions = config.getConfigurationSection("bosses."+key +".minion");
                        double mhealth = minions.getDouble("health");
                        int amt = minions.getInt("amount");
                        double dmod = minions.getDouble("damageMod");
                        Minion minion = new Minion(mhealth,dmod,amt,parsePotionEffects(minions));
                        add.setMinion(minion);
                    }
                    
                    ConfigurationSection mount = config.getConfigurationSection("bosses." + key + ".mount");
                    if(mount != null){
                        add.setMount(new Mount(mount.getDouble("health"),mount.getDouble("damageMod"),parsePotionEffects(mount),parseArmor(mount.getConfigurationSection("armor")),parseHand(mount),EntityType.valueOf(mount.getString("type"))));
                    }
                    
                    bosses.add(add);
                    getLogger().info("registered boss: " + add.getId());
                }
            }
        }));

        registerResource(new YAMLFile(this, "urns.yml", new ResourceReloadHook() {
            @Override
            public void onReload(ResourceFile file) {
                FileConfiguration config = ((YAMLFile)file).getConfig();
                for(String key : config.getKeys(false)){
                    Urn ret = new Urn();
                    ret.setType(key);
                    RandomCollection<Boss> collection = new RandomCollection<>();
                    ItemStack icon;

                    ConfigurationSection iSection = config.getConfigurationSection(key + ".icon");
                    icon = new ItemStack(Material.valueOf(iSection.getString("material")),1,(short)iSection.getInt("data"));
                    ItemMeta meta = icon.getItemMeta();
                    meta.setDisplayName(Colors.colorify(iSection.getString("displayName")));
                    List<String> lore = new ArrayList<String>();
                    for(String s : iSection.getStringList("lore")){
                        lore.add(Colors.colorify(s));
                    }
                    meta.setLore(lore);
                    icon.setItemMeta(meta);
                    ret.setIcon(icon);

                    ConfigurationSection bossChances = config.getConfigurationSection(key + ".chances");
                    for(String boss : bossChances.getKeys(false)){
                        Boss b = getBoss(boss);
                        if(b == null){
                            throw new IllegalArgumentException(boss + " is not a registered boss type.");
                        }
                        collection.add(bossChances.getDouble(boss),b);
                    }
                    RandomCollection<List<String>> itemChances = new RandomCollection<>();
                    ConfigurationSection itemDrops = config.getConfigurationSection(key + ".commandChances");
                    for(String item : itemDrops.getKeys(false)){
                        itemChances.add(itemDrops.getDouble(item + ".chance"),itemDrops.getStringList(item + ".commands"));
                    }
                    ret.setItemChances(itemChances);
                    ret.setChances(collection);
                    urns.add(ret);
                }
            }
        }));
        registerEvents(new BossListener());
        registerCmd(BossSpawnCmd.class);
        registerCmd(UrnCmd.class);
        reload();

        runScheduled(new BossDespawnTask(), ThreadLevel.SYNC,new TimePeriod(1, TimeUnit.SECONDS),new TimePeriod(1,TimeUnit.SECONDS));
    }

    @Override
    public void onModuleDisable(){
        Iterator<LivingBoss> livingBossIterator = PixelBosses.getI().getLivingBosses().iterator();
        while(livingBossIterator.hasNext()){
            LivingBoss b = livingBossIterator.next();
            b.getEntity().remove();
            if(b.getLivingMinions() != null){
                b.getLivingMinions().forEach(m -> m.remove());
            }
            if(b.getMount() != null) b.getMount().remove();
            livingBossIterator.remove();
        }
    }

    public Boss getBoss(String id){
        for(Boss boss : bosses){
            if(boss.getId().equalsIgnoreCase(id)) return boss;
        }
        return null;
    }

    public Urn getUrn(String type){
        for(Urn urn : urns)
            if(urn.getType().equalsIgnoreCase(type)) return urn;
        return null;
    }

    private Set<PotionEffect> parsePotionEffects(ConfigurationSection section){
        Set<PotionEffect> ret = new HashSet<>();
        for(String s : section.getStringList("potionEffects")){
            String[] split = s.split(":");
            PotionEffect e = new PotionEffect(PotionEffectType.getByName(split[0]),Integer.MAX_VALUE, Integer.parseInt(split[1]));
            ret.add(e);
        }
        return ret;
    }
    
    private ItemStack[] parseArmor(ConfigurationSection section){
        if(section != null){
            ItemStack head = new ItemStack(Material.AIR);
            ItemStack chest = new ItemStack(Material.AIR);
            ItemStack leggings = new ItemStack(Material.AIR);
            ItemStack boots = new ItemStack(Material.AIR);

            if(!section.getString("head").equalsIgnoreCase("AIR")){

                String[] hEnchants = section.getString("head").split(":");
                //getLogger().info("head: " + hEnchants[0]);
                short data;
                try{
                    data = (short)Integer.parseInt(hEnchants[0]);
                    head = new ItemStack(Material.valueOf(hEnchants[1]),1,data);
                    if(hEnchants.length > 2){
                        head.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(hEnchants[2])), Integer.parseInt(hEnchants[3]));
                    }
                }catch (NumberFormatException e){
                    head = new ItemStack(Material.valueOf(hEnchants[0]));
                    if(hEnchants.length > 1){
                        head.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(hEnchants[1])), Integer.parseInt(hEnchants[2]));
                    }
                }
            }
            if(!section.getString("chest").equalsIgnoreCase("AIR")){
                String[] hEnchants = section.getString("chest").split(":");
                //getLogger().info("chest: " + hEnchants[0]);
                short data;
                try{
                    data = (short)Integer.parseInt(hEnchants[0]);
                    chest = new ItemStack(Material.valueOf(hEnchants[1]),1,data);
                    if(hEnchants.length > 2){
                        chest.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(hEnchants[2])), Integer.parseInt(hEnchants[3]));
                    }
                }catch (NumberFormatException e){
                    chest = new ItemStack(Material.valueOf(hEnchants[0]));
                    if(hEnchants.length > 1){
                        chest.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(hEnchants[1])), Integer.parseInt(hEnchants[2]));
                    }
                }
            }
            if(!section.getString("leggings").equalsIgnoreCase("AIR")){
                String[] hEnchants = section.getString("leggings").split(":");
                short data;
                try{
                    data = (short)Integer.parseInt(hEnchants[0]);
                    leggings = new ItemStack(Material.valueOf(hEnchants[1]),1,data);
                    if(hEnchants.length > 2){
                        leggings.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(hEnchants[2])), Integer.parseInt(hEnchants[3]));
                    }
                }catch (NumberFormatException e){
                    leggings = new ItemStack(Material.valueOf(hEnchants[0]));
                    if(hEnchants.length > 1){
                        leggings.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(hEnchants[1])), Integer.parseInt(hEnchants[2]));
                    }
                }
            }
            if(!section.getString("boots").equalsIgnoreCase("AIR")){
                String[] hEnchants = section.getString("boots").split(":");
                short data;
                try{
                    data = (short)Integer.parseInt(hEnchants[0]);
                    boots = new ItemStack(Material.valueOf(hEnchants[1]),1,data);
                    if(hEnchants.length > 2){
                        boots.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(hEnchants[2])), Integer.parseInt(hEnchants[3]));
                    }
                }catch (NumberFormatException e){
                    boots = new ItemStack(Material.valueOf(hEnchants[0]));
                    if(hEnchants.length > 1){
                        boots.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(hEnchants[1])), Integer.parseInt(hEnchants[2]));
                    }
                }
            }
            return new ItemStack[]{head,chest,leggings,boots};
        }else{
            return new ItemStack[]{new ItemStack(Material.AIR),new ItemStack(Material.AIR),new ItemStack(Material.AIR),new ItemStack(Material.AIR)};
        }
    }

    private ItemStack parseHand(ConfigurationSection section){
        //getLogger().info("HAND ITEM: " + section.getString("hand"));
        String[] hEnchants = section.getString("hand").split(":");
        /*if(Material.valueOf(section.getString("hand")) != null) {
            return new ItemStack(Material.valueOf(section.getString("hand")));
        }else{
            return new ItemStack(Material.AIR);
        }*/
        ItemStack boots;
        short data;
        try{
            data = (short)Integer.parseInt(hEnchants[0]);
            boots = new ItemStack(Material.valueOf(hEnchants[1]),1,data);
            if(hEnchants.length > 2){
                boots.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(hEnchants[2])), Integer.parseInt(hEnchants[3]));
            }
        }catch (NumberFormatException e){
            boots = new ItemStack(Material.valueOf(hEnchants[0]));
            if(hEnchants.length > 1){
                boots.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(hEnchants[1])), Integer.parseInt(hEnchants[2]));
            }
        }
        return boots;
    }
}
