package pw.soopr.pixelbosses;

import org.bukkit.entity.Zombie;
import pw.soopr.pixelbosses.objects.LivingBoss;

import java.util.Iterator;

/**
 * Created by Aaron.
 */
public class BossDespawnTask implements Runnable{
    @Override
    public void run() {
        Iterator<LivingBoss> iterator = PixelBosses.getI().getLivingBosses().iterator();
        while(iterator.hasNext()){
            LivingBoss b = iterator.next();
            if(b.getEntity() == null || !b.getEntity().isValid()){
                b.getTemplate().spawnWithoutAddition(b,b.getLastLocation()).setHealth(b.getCurrentHealth());
            }else{
                b.setCurrentHealth(b.getEntity().getHealth());
                if(!checkLife(b)) iterator.remove();
            }
        }
    }

    private boolean checkLife(LivingBoss b){
        if(b.getExpiryDate() < System.currentTimeMillis()){
            if(b.getLivingMinions() != null){
                for(Zombie z : b.getLivingMinions()) z.remove();
            }
            if(b.getMount() != null){
                b.getMount().remove();
            }
            b.getEntity().remove();
            return false;
        }
        return true;
    }
}
